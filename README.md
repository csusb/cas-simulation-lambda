# AWS Lambda request handler for cas-simulation

## Requirements

A linux-y environment with Python 3 (tested with 3.7) and `make` and `zip`

## Building the function.zip

```make zip```

## Configuring lambda

   * Name: `check_cas_sp` (as an example)
   * Code entry type: Upload a .zip file
   * Handler: `lambda_function.lambda_handler` (which should be the default)
   * Trigger: API Gateway
      * POST
      * Require API Key: Required
      * Integration type: Lambda Function
          * Use Lambda Proxy integration: true
          * Lambda function: `check_cas_sp`  (match the Name given for the Lambda)
     * Usage Plans
          * Make sure one or more API keys are enabled for one or more environments

## Calling
   ```
   curl -X POST --data @example-request.json -H 'x-api-key: [redacted]' 'https://[generated_name].execute-api.us-east-1.amazonaws.com/default/check_cas_sp'
   ```

   Or, using the `--via-lambda` argument to the check_cas_sp Nagios plugin provdied by https://bitbucket.org/csusb/cas-simulation
