venv: venv/bin/activate

venv/bin/activate: requirements.txt
	test -d venv || python3 -m venv venv
	. venv/bin/activate; pip install -Ur requirements.txt
	touch venv/bin/activate

test: venv
	. venv/bin/activate

clean:
	rm -rf venv
	rm function.zip
	find -iname "*.pyc" -delete

function.zip: venv requirements.txt
	cd venv/lib/python3.?/site-packages && zip -r9 ../../../../function.zip .

zip: function.zip lambda_function.py
	zip --grow function.zip lambda_function.py
