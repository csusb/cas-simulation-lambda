from __future__ import print_function

import logging
import os
import json
import time
import traceback
import sys

from urllib.request import urlopen

import cas_simulation

logging.getLogger("cas_simulation").setLevel(logging.INFO)
logging.getLogger("cas_simulation.url_error_browser").setLevel(logging.INFO)

def lambda_handler(event, context):

    ip_address = 'unknown'
    try:
        ip_address = urlopen('http://checkip.amazonaws.com').read().strip().decode('utf-8')
    except OSError:
        pass

    for key in 'url', 'username', 'password', 'expression':
        if key not in event:
            return {
                 "error": f"The event argument was missing required '{key}' key"
            }

    post_auth_checks = event.get('post_auth_checks',[])
    skip_form_id = event.get('skip_form_id',None)
    warn_after = event.get('warn_after',10)
    if not isinstance(warn_after,float):
        warn_after = float(warn_after)

    try:
        duration = time.time()
        simulation = cas_simulation.cas_simulation(event['url'],
                                                   event['username'],
                                                   event['password'],
                                                   event['expression'],
                                                   post_auth_checks=post_auth_checks,
                                                   skip_form_id=skip_form_id)
        success = simulation.run()
        duration = time.time() - duration # wallclock diff as floating point
    except Exception as e:
        msg = "CRITICAL %s via %s\n" % (e,ip_address)
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_tb(exc_traceback, file=sys.stderr)
        try:
            msg += "Last URL: %s\n" % e.geturl()
        except AttributeError:
            pass
        try:
            msg += "Reason: %s\n" % e.reason
        except AttributeError:
            pass
        try:
            e.seek(0)
            msg += "Page Excerpt: %s\n" % e.read(1024*4)
        except:
            msg += "Page Excerpt: (n/a)\n"
        return {
             "error": msg
        }

    if not success:
        msg = "CRITICAL %s failed via %s\n" % (event['url'],ip_address)
        try:
            msg = "Last URL: %s\n" % e.geturl()
        except AttributeError:
            pass
        return {
             "state": "CRITICAL",
             "info": msg
        }

    if duration > warn_after:
        slow_duration,slow_url = simulation.slowest
        msg = "WARN CAS login for %s took %0.2fs via %s, slowest was %0.2fs for %s\n" % (event['url'],duration,ip_address,slow_duration,slow_url)
        return {
             "state": "WARNING",
             "info": msg
        }

    msg = "OK CAS login %s authenticated as %s - took %0.2fs via %s" % (event['url'], event['username'], duration,ip_address)
    return {
        "state": "OK",
        "info": msg
    }
